module.exports = {
  style: {
    postcssOptions: {},
  },
  eslint: {
    enable: false,
  },
  webpack: {
    configure: {
      ignoreWarnings: [{ message: /Failed to parse source map/ }],
    },
  },
};
