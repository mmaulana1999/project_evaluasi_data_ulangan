/* eslint-disable import/extensions */
/* eslint-disable import/prefer-default-export */
import axios from 'axios';
import { useEffect, useState } from 'react';
import Tables from './component/Table';

export const Crud = () => {
  const [data, setData] = useState();
  useEffect(() => {
    getUsers()
  }, []);
  function getUsers() {
    axios
      .get(`https://jsonplaceholder.typicode.com/users/1/todos`)
      .then((res) => {
        console.log(res);
        setData(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }

  return (
    <div>
      <Tables data={data} getUsers={getUsers} />
      {/* <ul>
        {data?.map((item) => {
          return <li>{item?.title}</li>;
        })}
      </ul> */}
    </div>
  );
};
