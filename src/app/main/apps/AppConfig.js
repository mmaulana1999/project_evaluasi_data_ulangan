import AdminConfig from './admin/AdminConfig';
import DashboardConfig from './dashboard/DashboardConfig';
import InfinityConfig from './infinityScrolling/InfinityConfig';
import CrudConfig from './latihanCrud/CrudConfig';
import ReactLazyLoadConfig from './reactLazyLoad/ReactLazyLoadConfig';
import SuperAdminConfig from './superAdmin/SuperAdminConfig';

const appsConfigs = [
  CrudConfig,
  InfinityConfig,
  ReactLazyLoadConfig,
  DashboardConfig,
  AdminConfig,
  SuperAdminConfig,
];

export default appsConfigs;
