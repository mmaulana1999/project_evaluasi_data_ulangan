/* eslint-disable import/prefer-default-export */
import { Paper } from '@mui/material';
import Chart from 'react-apexcharts';

export const Layout2SuperAdmin = () => {
  const myOptions = {
    chart: {
      height: 350,
      type: 'line',
      stacked: true,
    },
    stroke: {
      curve: 'smooth',
    },
    markers: {
      size: 0.001,
    },
    xaxis: {
      type: 'datetime',
      categories: [
        '01/01/2003',
        '02/01/2003',
        '03/01/2003',
        '04/01/2003',
        '05/01/2003',
        '06/01/2003',
        '07/01/2003',
        '08/01/2003',
        '09/01/2003',
        '10/01/2003',
        '11/01/2003',
      ],
    },
    tooltip: {
      // enabled: true,
      shared: true,
      // inverseOrder: false,
      // fillSeriesColor: false
      // custom: function ({ series, seriesIndex, dataPointIndex, w }) {
      //   return (
      //     '<div class="arrow_box">' +
      //     "<span>" +
      //     w.globals.labels[dataPointIndex] +
      //     ": " +
      //     series[seriesIndex][dataPointIndex] +
      //     "</span>" +
      //     "</div>"
      //   );
      // }
    },
  };

  const mySeries = [
    {
      name: 'TEAM B',
      type: 'area',
      data: [44, 55, 41, 67, 22, 43, 21, 41, 56, 27, 43],
    },
    {
      name: 'TEAM C',
      type: 'line',
      data: [30, 25, 36, 30, 45, 35, 64, 52, 59, 36, 39],
    },
    {
      name: 'TEAM A',
      type: 'area',
      data: [23, 11, 22, 27, 13, 22, 37, 21, 44, 22, 30],
    },
  ];
  return (
    <div className="">
      <Paper>
        <div className="m-5">
          <Chart width={1000} options={myOptions} series={mySeries} />
        </div>
      </Paper>
    </div>
  );
};
