/* eslint-disable import/prefer-default-export */
import Layout1SuperAdmin from './layout1/Layout1SuperAdmin';
import { Layout2SuperAdmin } from './layout2/Layout2SuperAdmin';
import Layout3SuperAdmin from './layout3/Layout3SuperAdmin';

export const SuperAdminPage = () => {
  return (
    <div className="grid grid-cols-3 gap-3 mr-10">
      <div className="col-span-3">
        <Layout1SuperAdmin />
      </div>
      <div className="m-10 ml-20 col-span-2">
        <Layout2SuperAdmin />
      </div>
      <div className="">
        <Layout3SuperAdmin />
      </div>
    </div>
  );
};
