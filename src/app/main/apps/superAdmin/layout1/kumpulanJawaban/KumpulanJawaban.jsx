/* eslint-disable no-shadow */
import * as React from 'react';
import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import { Typography } from '@mui/material';
import { useState, useEffect } from 'react';
import axios from 'axios';
import { toast } from 'react-toastify';
import FuseLoading from '@fuse/core/FuseLoading';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import ListItem from '@mui/material/ListItem';
import List from '@mui/material/List';
import Divider from '@mui/material/Divider';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import Slide from '@mui/material/Slide';

const columns = [
  { id: 'indexs', label: 'NO', minWidth: 20 },
  { id: 'namaSiswa', label: 'Nama Siswa', minWidth: 170 },
  { id: 'mataPelajaran', label: 'Mata Pelajaran', minWidth: 100 },
  {
    id: 'kelas',
    label: 'Kelas',
    minWidth: 170,
    align: 'right',
    format: (value) => value.toLocaleString('en-US'),
  },
  {
    id: 'nilaiPG',
    label: 'Nilai PG',
    minWidth: 170,
    align: 'right',
    format: (value) => value.toLocaleString('en-US'),
  },
  {
    id: 'date',
    label: 'Date',
    minWidth: 170,
    align: 'right',
    format: (value) => value.toLocaleString('en-US'),
  },
];
const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

function createData(indexs, namaSiswa, mataPelajaran, kelas, nilaiPG, date) {
  // cons  = kelas / nilaiPG;
  return { indexs, namaSiswa, mataPelajaran, kelas, nilaiPG, date };
}

export default function KumpulanJawaban() {
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [data, setData] = useState();
  const [dataJawaban, setDataJawaban] = useState();
  const [loading, setLoading] = useState();
  const [loadingJawaban, setloadingJawaban] = useState();

  const [open, setOpen] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };
  const getData = async () => {
    setLoading(false);
    const response = await axios
      .get(`${process.env.REACT_APP_API_URL_SOAL_DATA_SISWA}`)
      .then((res) => {
        console.log(res, 'dummy');
        setData(res?.data);
        setLoading(true);
      })
      .catch((err) => {
        console.log(err);
        setLoading(true);
      });
  };
  useEffect(() => {
    getData();
  }, []);

  const HandleDelete = async (id) => {
    console.log('hapus');
    setLoading(false);
    const response = await axios
      .delete(`${process.env.REACT_APP_API_URL_SOAL_DATA_SISWA}${id}/`)
      .then((res) => {
        setLoading(true);
        console.log(res, 'dummyDELETE');
        toast.success('Data Berhasil dihapus!');
        getData();
        // setNum(num - 1);
      })
      .catch((err) => {
        setLoading(true);
        console.log(err);
      });
  };
  const datas = data?.map((item, index) =>
    createData(
      index + 1,
      item?.namaSiswa,
      item?.MataPelajaran,
      item?.kelas,
      item?.nilaiPG,
      item?.waktuBuat
    )
  );
  const getDataJawaban = async () => {
    setloadingJawaban(true);
    const response = await axios
      .get(`https://633fa89dd1fcddf69ca6ec51.mockapi.io/jabawan/v1/jawabanEssay`)
      .then((res) => {
        setloadingJawaban(false);
        console.log(res, 'getDataJawaban');
        setDataJawaban(res.data);
      })
      .catch((err) => {
        console.log(err);
        setloadingJawaban(false);
      });
    setloadingJawaban(false);
  };

  useEffect(() => {
    getDataJawaban();
  }, []);

  return (
    <Paper sx={{ width: '100%' }}>
      {loading !== true ? (
        <div className="">
          <FuseLoading />
        </div>
      ) : (
        <TableContainer sx={{ maxHeight: 440 }}>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                <TableCell align="center" colSpan={6}>
                  <Typography variant="h5">JAWABAN SISWA</Typography>
                </TableCell>
              </TableRow>

              <TableRow>
                {columns.map((column) => (
                  <TableCell
                    key={column.id}
                    align={column.align}
                    style={{ top: 57, minWidth: column.minWidth }}
                  >
                    {column.label}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>

            <TableBody>
              {datas?.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row) => {
                return (
                  <TableRow hover role="checkbox" tabIndex={-1}>
                    {columns.map((column) => {
                      const value = row[column.id];
                      return (
                        <TableCell
                          className="cursor-pointer"
                          onClick={handleClickOpen}
                          key={column.id}
                          align={column.align}
                        >
                          {column.format && typeof value === 'number'
                            ? column.format(value)
                            : value}
                        </TableCell>
                      );
                    })}
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </TableContainer>
      )}
      <TablePagination
        rowsPerPageOptions={[5, 10, 25]}
        component="div"
        count={datas?.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
      <div>
        <Dialog fullScreen open={open} onClose={handleClose} TransitionComponent={Transition}>
          <AppBar sx={{ position: 'relative' }}>
            <Toolbar>
              <IconButton edge="start" color="inherit" onClick={handleClose} aria-label="close">
                <CloseIcon />
              </IconButton>
              <Typography sx={{ ml: 2, flex: 1 }} variant="h6" component="div">
                Keluar
              </Typography>
              <Button autoFocus color="inherit" onClick={handleClose}>
                save
              </Button>
            </Toolbar>
          </AppBar>
          <List>
            {/* {datas?.map((items) => (
              <div className="flex justify-center items-center">
                <Typography variant="h4">{items?.namaSiswa}</Typography>
              </div>
              
            ))} */}
            <div className="flex justify-center items-center">
              <Typography variant="h4">Muhamad Maulana</Typography>
            </div>
            <Divider />
            {dataJawaban?.map((item, index) => (
              <ListItem className="block" button>
                <Typography variant="h6" className="mb-10">
                  {index + 1}). {item?.JawabanEssay1}
                </Typography>
                <Typography variant="h6" className="mb-10">
                  {index + 2}). {item?.JawabanEssay2}
                </Typography>
                <Typography variant="h6" className="mb-10">
                  {index + 3}). {item?.JawabanEssay3}
                </Typography>
                <Typography variant="h6" className="mb-10">
                  {index + 4}). {item?.JawabanEssay4}
                </Typography>
                <Typography variant="h6" className="mb-10">
                  {index + 5}). {item?.JawabanEssay5}
                </Typography>
              </ListItem>
            ))}
          </List>
        </Dialog>
      </div>
    </Paper>
  );
}
