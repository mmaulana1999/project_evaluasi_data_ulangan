import { Alert, AlertTitle } from '@mui/material';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import { Link } from 'react-router-dom';
import HeaderAdmin from '../fakeDataAdmin/HeaderAdmin';

export default function Layout1SuperAdmin() {
  return (
    <div className="m-20">
      <Alert severity="success">
        <AlertTitle>Login Success</AlertTitle>
        Muhamad Maulana — <strong>123456789</strong>
      </Alert>
      <div className="flex flex-wrap mt-20 justify-between">
        {HeaderAdmin.map((item) => (
          <div className="">
            <Card className="flex mb-10" sx={{ minWidth: 275 }}>
              <div className="flex-none w-224 h-136">
                <CardContent>
                  <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
                    {item?.labels}
                  </Typography>
                  <Typography variant="h5" component="div">
                    {item?.label}
                  </Typography>
                </CardContent>
                <CardActions>
                  <Button size="small">Selanjutnya</Button>
                </CardActions>
              </div>
              <Link to={item?.path}>
                <div className="grow h-14">
                  <CardActions>{item?.icon}</CardActions>
                </div>
              </Link>
            </Card>
          </div>
        ))}
      </div>
    </div>
  );
}
