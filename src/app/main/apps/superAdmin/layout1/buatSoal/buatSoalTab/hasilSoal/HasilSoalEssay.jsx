/* eslint-disable no-nested-ternary */
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import { Button, Typography } from '@mui/material';
import * as React from 'react';
import { useFormControl } from '@mui/material/FormControl';
import FormHelperText from '@mui/material/FormHelperText';
import axios from 'axios';
import { useState, useEffect } from 'react';
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import FuseLoading from '@fuse/core/FuseLoading';

function MyFormHelperText() {
  const { focused } = useFormControl() || {};

  const helperText = React.useMemo(() => {
    if (focused) {
      return 'This field is being focused';
    }

    return 'Helper text';
  }, [focused]);

  return <FormHelperText>{helperText}</FormHelperText>;
}

export default function HasilSoalEssay({ dataSoal }) {
  const [data, setData] = useState(dataSoal);
  const [loading, setLoading] = useState(false);

  const getData = async () => {
    setLoading(true);
    const response = await axios
      .get(`${process.env.REACT_APP_API_URL_SOAL_DUMMY_ESSAY}`)
      .then((res) => {
        setLoading(true);
        console.log(res, 'dummy');
        setData(res.data);
      })
      .catch((err) => {
        setLoading(false);
        console.log(err);
      });
    setLoading(false);
  };
  useEffect(() => {
    getData();
  }, []);

  const HandleDelete = async (id) => {
    setLoading(true);
    console.log('hapus');
    const response = await axios
      .delete(`${process.env.REACT_APP_API_URL_SOAL_DUMMY_ESSAY}${id}/`)
      .then((res) => {
        setLoading(false);
        console.log(res, 'dummyDELETE');
        toast.success('Data Berhasil dihapus!');
        getData();
      })
      .catch((err) => {
        toast.error('Data Gagal dihapus!');
        setLoading(false);
        console.log(err);
      });
  };
  return (
    <List sx={{ width: '100%', maxWidth: '100%', bgcolor: 'background.paper' }}>
      {loading === true ? (
        <div>
          <FuseLoading />
        </div>
      ) : !loading === true ? (
        <div>
          {data?.map((item, index) => (
            <ListItem className="block" key={item} disableGutters>
              <div className="flex justify-between">
                <div className="flex">
                  <Typography variant="h6">{index + 1})</Typography>
                  <Typography variant="h6">. {item?.buatEssay}</Typography>
                </div>
                <div className="w-1/5 flex gap-5">
                  <Button disabled onClick={(e) => HandleDelete(item.id, e)} variant="contained">
                    Edit Soal Ke {index + 1}
                  </Button>
                  <Button onClick={(e) => HandleDelete(item.id, e)} variant="contained">
                    Hapus Soal Ke {index + 1}
                  </Button>
                </div>
              </div>
            </ListItem>
          ))}
        </div>
      ) : (
        <div>Soal Kosong</div>
      )}
      <ToastContainer autoClose={2000} />
      {/* <div className="m-10 flex justify-end">
        <Button variant="contained">Kirim</Button>
      </div> */}
    </List>
  );
}
