/* eslint-disable no-sequences */
/* eslint-disable no-unused-expressions */
/* eslint-disable no-undef */
/* eslint-disable import/prefer-default-export */
import { Alert, Button, Checkbox, TextField, Typography } from '@mui/material';
import axios from 'axios';
import { useState } from 'react';
import { toast, ToastContainer } from 'react-toastify';
import BuatCatatan from './BuatCatatan';
import LihatSoal from './LihatSoal';

export const BuatPilihanGanda = ({ dataSoal }) => {
  const [checkedA, setCheckedA] = useState([true, false]);
  const [checkedB, setCheckedB] = useState([true, false]);
  const [checkedC, setCheckedC] = useState([true, false]);
  const [checkedD, setCheckedD] = useState([true, false]);
  const [checkedE, setCheckedE] = useState([true, false]);
  const [soalPG, setSoalPG] = useState('');
  const [pilihanA, setpilihanA] = useState('');
  const [pilihanB, setpilihanB] = useState('');
  const [pilihanC, setpilihanC] = useState('');
  const [pilihanD, setpilihanD] = useState('');
  const [pilihanE, setpilihanE] = useState('');
  const [num, setNum] = useState(1);

  const handleChangeA = (event) => {
    setCheckedA(event.target.checked);
  };
  const handleChangeB = (event) => {
    setCheckedB(event.target.checked);
  };
  const handleChangeC = (event) => {
    setCheckedC(event.target.checked);
  };
  const handleChangeD = (event) => {
    setCheckedD(event.target.checked);
  };
  const handleChangeE = (event) => {
    setCheckedE(event.target.checked);
    console.log(event.target.checked);
  };

  const handleKirim = (e) => {
    setNum(num + 1);
  };
  const handleKembali = (e) => {
    setSoalPG('');
    setpilihanA('');
    setpilihanB('');
    setpilihanC('');
    setpilihanD('');
    setpilihanE('');
  };
  const getData = async () => {
    const response = await axios
      .get(`${process.env.REACT_APP_API_URL_SOAL_DUMMY_PILIHAN_GANDA}`)
      .then((res) => {
        console.log(res, 'dummy');
        setData(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const HandleSubmit = async (event) => {
    event.preventDefault();
    const response = await axios
      .post(`${process.env.REACT_APP_API_URL_SOAL_DUMMY_PILIHAN_GANDA}`, {
        soalPG,
        pilihanA,
        pilihanB,
        pilihanC,
        pilihanD,
        pilihanE,
      })
      .then((res) => {
        console.log(res, 'dummyPOST');
        Alert('terkirim');
        toast.success('Data Berhasil Di Tambahkan');
        getData();
        // setNum(num + 1);
      })
      .catch((err) => {
        console.log(err, 'err');
      });
    setSoalPG('');
    setpilihanA('');
    setpilihanB('');
    setpilihanC('');
    setpilihanD('');
    setpilihanE('');
    handleKirim();
  };

  const disableKirim = () => {
    soalPG === '' ||
      pilihanA === '' ||
      pilihanB === '' ||
      pilihanC === '' ||
      pilihanD === '' ||
      pilihanE === '';
  };

  return (
    <div>
      <div className="flex justify-end m-10">
        <div className="mr-10">
          <LihatSoal num={num} setNum={setNum} dataSoal={dataSoal} />
        </div>
        <div>
          <BuatCatatan />
        </div>
      </div>
      <div className="flex items-center">
        <Typography className="mr-5" variant="h6">
          {num}.
        </Typography>
        <TextField
          value={soalPG}
          onChange={(e) => setSoalPG(e.target.value)}
          placeholder="Tulis Soal"
          label="Buat Soal"
          fullWidth
        />
      </div>
      <div className="ml-20 mt-10">
        <ul>
          <li className="flex items-center">
            <Checkbox
              checked={checkedA}
              onChange={handleChangeA}
              inputProps={{ 'aria-label': 'controlled' }}
            />
            <Typography className=" mt-20 mr-20" variant="h6">
              A.
            </Typography>
            <TextField
              value={pilihanA}
              onChange={(e) => setpilihanA(e.target.value)}
              size="small"
              fullWidth
              placeholder="Tulis Soal"
              label="Buat Soal A"
            />
          </li>
          <li className="flex items-center">
            <Checkbox
              checked={checkedB}
              onChange={handleChangeB}
              inputProps={{ 'aria-label': 'controlled' }}
            />
            <Typography className=" mt-20 mr-20" variant="h6">
              B.
            </Typography>
            <TextField
              value={pilihanB}
              onChange={(e) => setpilihanB(e.target.value)}
              size="small"
              fullWidth
              placeholder="Tulis Soal"
              label="Buat Soal B"
            />
          </li>
          <li className="flex items-center">
            <Checkbox
              checked={checkedC}
              onChange={handleChangeC}
              inputProps={{ 'aria-label': 'controlled' }}
            />
            <Typography className=" mt-20 mr-20" variant="h6">
              C.
            </Typography>
            <TextField
              value={pilihanC}
              onChange={(e) => setpilihanC(e.target.value)}
              size="small"
              fullWidth
              placeholder="Tulis Soal"
              label="Buat Soal C"
            />
          </li>
          <li className="flex items-center">
            <Checkbox
              checked={checkedD}
              onChange={handleChangeD}
              inputProps={{ 'aria-label': 'controlled' }}
            />
            <Typography className=" mt-20 mr-20" variant="h6">
              D.
            </Typography>
            <TextField
              value={pilihanD}
              onChange={(e) => setpilihanD(e.target.value)}
              size="small"
              fullWidth
              placeholder="Tulis Soal"
              label="Buat Soal D"
            />
          </li>
          <li className="flex items-center">
            <Checkbox
              checked={checkedE}
              onChange={handleChangeE}
              inputProps={{ 'aria-label': 'controlled' }}
            />
            <Typography className=" mt-20 mr-20" variant="h6">
              E.
            </Typography>
            <TextField
              value={pilihanE}
              onChange={(e) => setpilihanE(e.target.value)}
              size="small"
              fullWidth
              placeholder="Tulis Soal"
              label="Buat Soal E"
            />
          </li>
        </ul>
      </div>
      <div className="flex justify-end m-20">
        <Button
          className="mr-10"
          onClick={handleKembali}
          disabled={
            soalPG === '' ||
            pilihanA === '' ||
            pilihanB === '' ||
            pilihanC === '' ||
            pilihanD === '' ||
            pilihanE === ''
          }
          variant="contained"
        >
          Reset
        </Button>
        <Button
          disabled={
            soalPG === '' ||
            pilihanA === '' ||
            pilihanB === '' ||
            pilihanC === '' ||
            pilihanD === '' ||
            pilihanE === ''
          }
          onClick={HandleSubmit}
          variant="contained"
        >
          Kirim
        </Button>
      </div>
      <ToastContainer autoClose={2000} />
    </div>
  );
};
