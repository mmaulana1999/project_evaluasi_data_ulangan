import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import axios from 'axios';
import { useEffect, useState } from 'react';
import { Button, TextField } from '@mui/material';
import * as React from 'react';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';

function createData(id, pilihanB, pilihanC, pilihanD, pilihanE) {
  return { id, pilihanB, pilihanC, pilihanD, pilihanE };
}

export default function TestApi() {
  const [data, setData] = useState();
  const [pilihanA, setpilihanA] = useState('');
  const [pilihanC, setpilihanC] = useState('');
  const [open, setOpen] = React.useState(false);
  const [openTambah, setOpenTambah] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const handleClickOpenTambah = () => {
    setOpenTambah(true);
  };

  const handleCloseTambah = () => {
    setOpenTambah(false);
  };
  // const [pilihanB, setpilihanB] = useState("");
  // useEffect(() => {
  const getData = async () => {
    const response = await axios
      .get(`${process.env.REACT_APP_API_URL_SOAL_DUMMY_PILIHAN_GANDA}`)
      .then((res) => {
        console.log(res, 'dummy');
        setData(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  // }, []);

  const HandleSubmit = async (event) => {
    event.preventDefault();
    const response = await axios
      .post(`${process.env.REACT_APP_API_URL_SOAL_DUMMY_PILIHAN_GANDA}`, {
        pilihanA,
        pilihanC,
      })
      .then((res) => {
        console.log(res, 'Tambah Soal');
        getData();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const HandleDelete = async (id) => {
    console.log('hapus');
    const response = await axios
      .delete(`${process.env.REACT_APP_API_URL_SOAL_DUMMY_PILIHAN_GANDA}${id}/`)
      .then((res) => {
        console.log(res, 'dummyDELETE');
        getData();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const HandleEdit = async (id) => {
    console.log('Edit');
    const body = { pilihanA: '', pilihanC: '' };
    const response = await axios
      .put(`${process.env.REACT_APP_API_URL_SOAL_DUMMY_PILIHAN_GANDA}${id}/`)
      .then((res) => {
        console.log(res, 'dummyEdit');
        getData();
      })
      .catch((err) => {
        console.log(err);
      });
  };
  useEffect(() => {
    // HandleDelete();
    getData();
  }, []);

  const rows = data?.map((item) =>
    createData(item?.id, item?.pilihanB, item?.pilihanC, item?.pilihanD, item?.pilihanE)
  );
  console.log(rows);

  return (
    <TableContainer component={Paper}>
      <div className="m-10 flex justify-end">
        <Button onClick={handleClickOpenTambah} variant="contained">
          Tambah
        </Button>
      </div>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Test 1</TableCell>
            <TableCell align="right">Test2</TableCell>
            <TableCell align="right">Test3</TableCell>
            <TableCell align="right">Test4</TableCell>
            <TableCell align="right">Test5</TableCell>
            <TableCell align="right">Action</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows?.map((row) => (
            <TableRow key={row.id} sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
              {/* {console.log(row)} */}
              <TableCell component="th" scope="row">
                {row.id}
              </TableCell>
              <TableCell align="right">{row.pilihanB}</TableCell>
              <TableCell align="right">{row.pilihanC}</TableCell>
              <TableCell align="right">{row.piLihanD}</TableCell>
              <TableCell align="right">{row.pilihanE}</TableCell>
              <TableCell align="right">
                <Button variant="contained" onClick={handleClickOpen}>
                  Edit
                </Button>
                <Button
                  className="m-5"
                  variant="contained"
                  onClick={(e) => HandleDelete(row.id, e)}
                >
                  Hapus
                </Button>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
      <div>
        <Dialog
          open={open}
          onClose={handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">Use Google's location service?</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              <div className="m-10">
                <TextField
                  value={pilihanA}
                  onChange={(e) => setpilihanA(e.target.value)}
                  label="pilihanA"
                />
                <TextField
                  value={pilihanC}
                  onChange={(e) => setpilihanC(e.target.value)}
                  label="pilihanB"
                />
              </div>
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose}>Batal</Button>
            <Button onClick={HandleEdit} autoFocus>
              Edit
            </Button>
          </DialogActions>
        </Dialog>
      </div>
      {/* edit */}
      <div>
        <Dialog
          open={openTambah}
          onClose={handleCloseTambah}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">Use Google's location service?</DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              <div className="m-10">
                <TextField
                  value={pilihanA}
                  onChange={(e) => setpilihanA(e.target.value)}
                  label="pilihanA"
                />
                <TextField
                  value={pilihanC}
                  onChange={(e) => setpilihanC(e.target.value)}
                  label="pilihanB"
                />
              </div>
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleCloseTambah}>Batal</Button>
            <Button onClick={HandleSubmit} autoFocus>
              Tambah
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    </TableContainer>
  );
}
