import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { CardActionArea } from '@mui/material';
import LazyLoad from 'react-lazyload';

export default function Cards({ data, load }) {
  console.log(data, 'data');
  return (
    <div className="flex flex-wrap m-5">
      {data?.map((item, index) => (
        <Card sx={{ maxWidth: 200 }} className="m-10">
          <CardActionArea>
            <LazyLoad>
              <CardMedia component="img" height="140" image={item?.image} alt="green iguana" />
            </LazyLoad>
            <CardContent>
              <Typography gutterBottom variant="h5" component="div">
                {item?.fullTitle}
              </Typography>
              <Typography variant="body2" color="text.secondary">
                Lizards are a widespread group of squamate reptiles, with over 6,000 species,
                ranging across all continents except Antarctica
              </Typography>
            </CardContent>
          </CardActionArea>
        </Card>
      ))}
    </div>
  );
}
