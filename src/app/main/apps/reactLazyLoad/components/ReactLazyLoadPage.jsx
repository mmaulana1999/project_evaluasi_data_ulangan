/* eslint-disable react-hooks/exhaustive-deps */
import axios from 'axios';
import { useState, useEffect } from 'react';
import Cards from './Cards';
import LoadingCards from './LoadingCards';

/* eslint-disable import/prefer-default-export */
export const ReactLazyLoadPage = () => {
  // const accesToken = `WbSJ8o15xDsavq-6r-d-CkmMKmKA9XXK1PVodBXvVrE`;
  const url = `https://imdb-api.com/en/API/Top250Movies/k_hd11bl91`;

  const [img, setImg] = useState();
  const [title, setTitle] = useState();
  const [judul, setJudul] = useState();
  const [data, setData] = useState();
  const [load, setLoad] = useState();

  useEffect(() => {
    setLoad(true);
    axios
      .get(url)
      .then((res) => {
        setLoad(false);
        setData(res.data.items);
        console.log(res);
      })
      .catch((err) => {
        setLoad(false);
        console.log(err);
      });
  }, []);
  if (!data) {
    return <LoadingCards dataLoading={250} />;
  }

  return (
    <div>
      <Cards data={data} load={load} />
    </div>
  );
};
