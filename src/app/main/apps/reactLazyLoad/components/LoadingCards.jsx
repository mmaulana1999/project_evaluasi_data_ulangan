/* eslint-disable no-unreachable */
import Skeleton from '@mui/material/Skeleton';
import Stack from '@mui/material/Stack';

export default function LoadingCards({ dataLoading }) {
  console.log(dataLoading, 'dataLoading');
  return (
    <div className="flex flex-wrap m-5">
      {Array(dataLoading)
        .fill(0)
        .map((_, items) => (
          <Stack spacing={1} className="m-10" key={items}>
            <Skeleton variant="rectangular" width={210} height={300} className="rounded-3xl" />
            <Skeleton variant="rounded" width={210} height={60} />
            <Skeleton variant="rounded" width={210} height={100} />
          </Stack>
        ))}
      ;
    </div>
  );
}
