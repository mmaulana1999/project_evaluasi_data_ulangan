/* eslint-disable import/no-duplicates */
import i18next from 'i18next';
import en from '../../example/i18n/en';
import tr from '../../example/i18n/en';
import ar from '../../example/i18n/en';
import { AdminPage } from './AdminPage';
import { BuatSoalPage } from './layout1/buatSoal/BuatSoalPage';
import { KumpulanJawabanPage } from './layout1/kumpulanJawaban/KumpulanJawabanPage';
import { AdminProfilPage } from './layout1/profil/AdminProfilPage';
import { AdminSettingsPage } from './layout1/settings/AdminSettingsPage';

i18next.addResourceBundle('en', 'examplePage', en);
i18next.addResourceBundle('tr', 'examplePage', tr);
i18next.addResourceBundle('ar', 'examplePage', ar);

const AdminConfig = {
  settings: {
    layout: {
      config: {},
    },
  },
  routes: [
    {
      path: 'admin',
      element: <AdminPage />,
    },
    {
      path: '/admin/buatSoal',
      element: <BuatSoalPage />,
    },
    {
      path: '/admin/profil',
      element: <AdminProfilPage />,
    },
    {
      path: '/admin/settings',
      element: <AdminSettingsPage />,
    },
    {
      path: '/admin/kumpulanJawaban',
      element: <KumpulanJawabanPage />,
    },
  ],
};

export default AdminConfig;
