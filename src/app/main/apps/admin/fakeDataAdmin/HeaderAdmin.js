import MessageIcon from '@mui/icons-material/Message';
import PermIdentityIcon from '@mui/icons-material/PermIdentity';
import GradingIcon from '@mui/icons-material/Grading';
import SettingsIcon from '@mui/icons-material/Settings';

const HeaderAdmin = [
  {
    label: 'Buat Soal',
    labels: 'Pesan',
    path: '/admin/buatSoal',
    icon: <GradingIcon fontSize="large" />,
  },
  {
    label: 'Profil',
    labels: 'Profil Kamu',
    path: '/admin/profil',
    icon: <PermIdentityIcon fontSize="large" />,
  },
  {
    label: 'Settings',
    labels: 'Ubah Pengaturan',
    path: '/admin/settings',
    icon: <SettingsIcon fontSize="large" />,
  },
  {
    label: 'Answer Set',
    labels: 'Himpunan Nilai',
    path: '/admin/kumpulanJawaban',
    icon: <MessageIcon fontSize="large" />,
  },
];

export default HeaderAdmin;
