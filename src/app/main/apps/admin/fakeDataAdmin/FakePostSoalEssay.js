const FakePostSoalEssay = [
  {
    id: 1,
    label: 'Buat Soal',
    labels: 'Pesan',
    path: '/admin/buatSoal',
  },
  {
    id: 2,
    label: 'Profil',
    labels: 'Profil Kamu',
    path: '/admin/profil',
  },
  {
    id: 3,
    label: 'Settings',
    labels: 'Ubah Pengaturan',
    path: '/admin/settings',
  },
  {
    id: 4,
    label: 'Message',
    labels: 'Himpunan Nilai',
    path: '/admin/message',
  },
  {
    id: 5,
    label: 'Message',
    labels: 'Himpunan Nilai',
    path: '/admin/message',
  },
];

export default FakePostSoalEssay;
