/* eslint-disable no-alert */
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { TextField } from '@mui/material';
import axios from 'axios';
import { useState, useEffect } from 'react';
import { toast } from 'react-toastify';

export default function BuatCatatan() {
  const [open, setOpen] = useState(false);
  const [data, setData] = useState();
  const [catatan, setCatatan] = useState('');
  const [loading, setLoading] = useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const getData = async () => {
    const response = await axios
      .get(`${process.env.REACT_APP_API_URL_SOAL_DUMMY_CATATAN}`)
      .then((res) => {
        console.log(res, 'dummy catatan');
        setData(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  useEffect(() => {
    getData();
  }, []);

  const HandleSubmit = async (event) => {
    event.preventDefault();
    const response = await axios
      .post(`${process.env.REACT_APP_API_URL_SOAL_DUMMY_CATATAN}`, {
        catatan,
      })
      .then((res) => {
        console.log(res, ' Tambah dummy catatan');
        getData();
        toast.success('Data Behasil Tambahkan');
        setCatatan('');
        // handleClose();
        // getData();
      })
      .catch((err) => {
        console.log(err, 'err');
      });
  };
  const HandleDelete = async (id) => {
    setLoading(true);
    console.log('hapus');
    const response = await axios
      .delete(`${process.env.REACT_APP_API_URL_SOAL_DUMMY_CATATAN}${id}`)
      .then((res) => {
        setLoading(false);
        console.log(res, 'delete Ctatan');
        toast.success('Data Berhasil dihapus!');
        getData();
        // handleClose();
      })
      .catch((err) => {
        toast.error('Data Gagal dihapus!');
        setLoading(false);
        console.log(err);
      });
  };

  return (
    <div>
      <div>
        <Button variant="outlined" onClick={handleClickOpen}>
          Buat Catatan
        </Button>
        <Dialog open={open} onClose={handleClose}>
          <DialogTitle> Buatlah catatan</DialogTitle>
          <DialogContent>
            {data?.map((item, index) => (
              <div className="m-10 flex justify-between">
                <div className="m-10">
                  <DialogContentText>
                    {index + 1}.) {item?.catatan}
                  </DialogContentText>
                </div>
                <div className="m-10 flex items-center">
                  <DialogContentText className="flex ">
                    <Button disabled className="mr-5" variant="contained">
                      Edit
                    </Button>
                    <Button variant="contained" onClick={(e) => HandleDelete(item.id, e)}>
                      Hapus
                    </Button>
                  </DialogContentText>
                </div>
              </div>
            ))}
            <TextField
              multiline
              rows={4}
              placeholder="Tulis Catatan"
              autoFocus
              margin="dense"
              id="name"
              label="Buat Catatan"
              type="email"
              fullWidth
              variant="standard"
              value={catatan}
              onChange={(e) => setCatatan(e.target.value)}
            />
          </DialogContent>
          <DialogActions>
            <Button variant="contained" onClick={handleClose}>
              Batal
            </Button>
            <Button disabled={catatan === ''} variant="contained" onClick={HandleSubmit}>
              Simpan
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    </div>
  );
}
