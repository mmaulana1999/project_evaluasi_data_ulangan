/* eslint-disable no-alert */
/* eslint-disable no-undef */
/* eslint-disable import/prefer-default-export */
import { Button, TextField, Typography } from '@mui/material';
import axios from 'axios';
import { useState, useEffect } from 'react';
import { toast, ToastContainer } from 'react-toastify';
import LihatSoalEssay from './LihatSoalEssay';
import 'react-toastify/dist/ReactToastify.css';

export const BuatEssay = ({ dataSoal }) => {
  const [buatEssay, setBuatEssay] = useState('');
  const [data, setData] = useState();
  const [checkedA, setCheckedA] = useState([true, false]);
  const [checkedB, setCheckedB] = useState([true, false]);
  const [checkedC, setCheckedC] = useState([true, false]);
  const [checkedD, setCheckedD] = useState([true, false]);
  const [checkedE, setCheckedE] = useState([true, false]);
  const [num, setNum] = useState(1);

  const handleChangeA = (event) => {
    setCheckedA(event.target.checked);
  };
  const handleChangeB = (event) => {
    setCheckedB(event.target.checked);
  };
  const handleChangeC = (event) => {
    setCheckedC(event.target.checked);
  };
  const handleChangeD = (event) => {
    setCheckedD(event.target.checked);
  };
  const handleChangeE = (event) => {
    setCheckedE(event.target.checked);
    console.log(event.target.checked);
  };

  const handleKirim = (e) => {
    setNum(num + 1);
  };
  const handleKembali = (e) => {
    setNum(num - 1);
  };
  const getData = async () => {
    const response = await axios
      .get(`${process.env.REACT_APP_API_URL_SOAL_DUMMY_ESSAY}`)
      .then((res) => {
        console.log(res, 'dummyEssays');
        setData(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  useEffect(() => {
    getData();
  }, []);

  const HandleSubmit = async (event) => {
    event.preventDefault();
    const response = await axios
      .post(`${process.env.REACT_APP_API_URL_SOAL_DUMMY_ESSAY}`, {
        buatEssay,
      })
      .then((res) => {
        console.log(res, 'dummyPOST');
        getData();
        toast.success('Data Behasil Tambahkan');
        setBuatEssay('');
        handleKirim();
        // getData();
      })
      .catch((err) => {
        console.log(err, 'err');
      });
  };

  return (
    <div>
      <ToastContainer autoClose={3000} />
      <div>
        <div className="flex justify-end m-10">
          <LihatSoalEssay dataSoal={data} />
        </div>
        <div className="flex items-center">
          <Typography className="mr-5" variant="h6">
            {num}
          </Typography>
          <TextField
            value={buatEssay}
            onChange={(e) => setBuatEssay(e.target.value)}
            label="Buat Soal"
            multiline
            rows={4}
            placeholder="Tulis Soal"
            fullWidth
          />
        </div>
        <div />
        <div className="flex justify-end m-20">
          <Button className="mr-10" onClick={handleKembali} disabled={num <= 1} variant="contained">
            Kembali
          </Button>
          <Button disabled={buatEssay === ''} onClick={HandleSubmit} variant="contained">
            Kirim
          </Button>
        </div>
      </div>
    </div>
  );
};
