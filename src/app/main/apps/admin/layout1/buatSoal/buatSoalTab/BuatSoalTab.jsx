/* eslint-disable no-alert */
import PropTypes from 'prop-types';
import SwipeableViews from 'react-swipeable-views';
import { useTheme } from '@mui/material/styles';
import AppBar from '@mui/material/AppBar';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Typography from '@mui/material/Typography';
import { Box } from '@mui/system';
import { useState, useEffect } from 'react';
import axios from 'axios';
import { BuatPilihanGanda } from './BuatPilihanGanda';
import { BuatEssay } from './BuatEssay';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `full-width-tab-${index}`,
    'aria-controls': `full-width-tabpanel-${index}`,
  };
}

export default function BuatSoalTab() {
  const theme = useTheme();
  const [value, setValue] = useState(0);
  const [dataSoal, setDataSoal] = useState();

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeIndex = (index) => {
    setValue(index);
  };

  // start Time

  const Test = () => {
    alert('Test1 BERHASIL');
  };
  // End Time
  const getData = async () => {
    const response = await axios
      .get(`${process.env.REACT_APP_API_URL_SOAL_DUMMY_PILIHAN_GANDA}`)
      .then((res) => {
        console.log(res, 'dataSoal');
        setDataSoal(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  useEffect(() => {
    getData();
  }, []);

  return (
    <div>
      <div className="w-full flex justify-center">
        <Typography variant="h4">BUAT SOAL</Typography>
      </div>

      <Box sx={{ bgcolor: 'background.paper', width: '100%' }}>
        <AppBar position="static">
          <Tabs
            value={value}
            onChange={handleChange}
            indicatorColor="secondary"
            textColor="inherit"
            variant="fullWidth"
            aria-label="full width tabs example"
          >
            <Tab label="Pilihan Ganda" {...a11yProps(0)} />
            <Tab label="Essay" {...a11yProps(1)} />
            {/* <Tab label="Status" {...a11yProps(2)} /> */}
          </Tabs>
        </AppBar>
        <SwipeableViews
          axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
          index={value}
          onChangeIndex={handleChangeIndex}
        >
          <TabPanel value={value} index={0} dir={theme.direction}>
            <BuatPilihanGanda dataSoal={dataSoal} />
          </TabPanel>
          <TabPanel value={value} index={1} dir={theme.direction}>
            <BuatEssay dataSoal={dataSoal} />
          </TabPanel>
          {/* <TabPanel value={value} index={2} dir={theme.direction}>
          Status
        </TabPanel> */}
        </SwipeableViews>
      </Box>
    </div>
  );
}
