/* eslint-disable import/prefer-default-export */

import styled from '@emotion/styled';
import FusePageSimple from '@fuse/core/FusePageSimple';
import { Typography } from '@mui/material';
import { Link } from 'react-router-dom';
import ReplyAllIcon from '@mui/icons-material/ReplyAll';
import KumpulanJawaban from './KumpulanJawaban';

const Root = styled(FusePageSimple)({
  '& .FusePageSimple-header': {},
  '& .FusePageSimple-toolbar': {},
  '& .FusePageSimple-content': {},
  '& .FusePageSimple-sidebarHeader': {},
  '& .FusePageSimple-sidebarContent': {},
});
export const KumpulanJawabanPage = () => {
  return (
    <Root
      header={
        <div className="w-full h-4/5 flex items-center">
          <div className="flex justify-start ">
            <Link to="/admin">
              <Typography variant="h6">
                <ReplyAllIcon fontSize="large" />
              </Typography>
            </Link>
          </div>
          <div className="w-full h-2/6">
            <div className="flex justify-center">
              <Typography variant="h4">Muhamad Maulana</Typography>
            </div>
            <div className="flex justify-center">
              <Typography variant="h6">1209387</Typography>
            </div>
          </div>
        </div>
      }
      contentToolbar={
        <div className="px-24">
          <Typography variant="h5">Pelajaran Yang Sudah Di Selesaikan</Typography>
        </div>
      }
      content={
        <div className="p-24">
          <Typography>Content</Typography>
          <KumpulanJawaban />
          <br />
        </div>
      }
    />
  );
};
