import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import Typography from '@mui/material/Typography';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import MenuItem from '@mui/material/MenuItem';
import Menu from '@mui/material/Menu';
import Slide from '@mui/material/Slide';
import { Card, CardContent, CardMedia, Divider } from '@mui/material';
import { styled } from '@mui/material/styles';
import MoreIcon from '@mui/icons-material/MoreVert';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import ProfilData2 from 'app/main/apps/dashboard/components/fakeData/ProfillData2';
import MataPelajaran from 'app/main/apps/dashboard/components/fakeData/MataPelajaran';
import ProfilData from 'app/main/apps/dashboard/components/fakeData/ProfilData';
import firstsnow from '../../fakeDataAdmin/dataImg/first-snow.jpg';
import EditProfil from './editProfil/EditProfil';

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const StyledToolbar = styled(Toolbar)(({ theme }) => ({
  alignItems: 'flex-start',
  paddingTop: theme.spacing(1),
  paddingBottom: theme.spacing(2),
  // Override media queries injected by theme.mixins.toolbar
  '@media all': {
    minHeight: 128,
  },
}));

export default function SimpleAccordion() {
  const [auth, setAuth] = React.useState(true);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [open, setOpen] = React.useState(false);
  const [data, setData] = ProfilData2;

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleCloseDialog = () => {
    setOpen(false);
  };

  //

  const handleChange = (event) => {
    setAuth(event.target.checked);
  };

  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };
  return (
    <div>
      <div className="flex m-10 gap-5">
        <div>
          <Button variant="contained">Test 1</Button>
        </div>
        <div>
          <Button variant="contained">Test 2</Button>
        </div>
      </div>
      <div className="grid grid-cols-3 gap-4">
        <div className="col-span-2  mr-10 w-full">
          {ProfilData.map((item) => (
            <div>
              <Accordion className="">
                <AccordionSummary
                  expandIcon={<ExpandMoreIcon />}
                  aria-controls="panel1a-content"
                  id="panel1a-header"
                >
                  <Typography>{item?.label}</Typography>
                </AccordionSummary>
                <AccordionDetails>
                  <div className="flex items-center">
                    <Typography>{item?.icon}</Typography>
                    <Typography className=" ml-5">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse malesuada
                      lacus ex, sit amet blandit leo lobortis eget.
                    </Typography>
                  </div>
                </AccordionDetails>
              </Accordion>
            </div>
          ))}
          <div className="mt-20">
            <div className=" mb-28">
              <Typography variant="h5">
                MUHAMAD MAULANA's Pelajaran yang Sudah Di Kerjakan
              </Typography>
            </div>
            <div>
              <div className="flex flex-wrap w-full">
                {MataPelajaran.slice(1, 5).map((item) => (
                  <div className="">
                    <div className="">
                      <div className="m-10">
                        <Card sx={{ display: 'flex' }}>
                          <Button>
                            <CardMedia
                              component="img"
                              sx={{ width: 151 }}
                              image={firstsnow}
                              alt="Mata Pelajaran"
                            />
                          </Button>
                          <Box className="w-224" sx={{ display: 'flex', flexDirection: 'column' }}>
                            <CardContent sx={{ flex: '1 0 auto' }}>
                              <Typography component="div" variant="h5">
                                Selesai
                              </Typography>
                            </CardContent>
                          </Box>
                        </Card>
                        <div className="m-10">
                          <div>
                            <ul>
                              <li>Nama Guru : {item?.namaGuru} </li>
                              <li>Kelas : {item?.Kelas} </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>

        <div className="">
          <div>
            <Box sx={{ flexGrow: 1 }}>
              <AppBar position="static">
                <StyledToolbar>
                  <Typography noWrap component="div" sx={{ flexGrow: 1, alignSelf: 'flex-end' }}>
                    <div className="mb-20">
                      <Typography variant="h4">Profil</Typography>
                    </div>
                    {ProfilData2.map((item) => (
                      <div className="">
                        <div>
                          <Typography className="opacity-75" variant="subtitle1">
                            First name
                          </Typography>
                          <Typography variant="h6">{item?.nama.toUpperCase()}</Typography>
                        </div>
                        <Divider />
                        <div>
                          <Typography className="opacity-75" variant="subtitle1">
                            NIS
                          </Typography>
                          <Typography variant="h6">{item?.nis}</Typography>
                        </div>
                        <Divider />
                        <div>
                          <Typography className="opacity-75" variant="subtitle1">
                            Preferred language
                          </Typography>
                          <Typography variant="h6">{item?.bahasa}</Typography>
                        </div>
                        <Divider />
                        <div>
                          <Typography className="opacity-75" variant="subtitle1">
                            First access to site
                          </Typography>
                          <Typography variant="h6">{item?.awalAkses}</Typography>
                        </div>
                        <Divider />
                        <div>
                          <Typography className="opacity-75" variant="subtitle1">
                            Last access to site
                          </Typography>
                          <Typography variant="h6">{item?.akhirAkses}</Typography>
                        </div>
                        <Divider />
                        <div>
                          <Typography className="opacity-75" variant="subtitle1">
                            Email address
                          </Typography>
                          <Typography variant="h6">{item?.email}</Typography>
                        </div>
                        <Divider />
                        <div className="border-2 p-10 mt-20">
                          <Typography variant="h5">*Aktifitas Terakhir</Typography>
                          <br />
                          <Typography className="opacity-75" variant="subtitle1">
                            Mata Pelajaran Yang Selesai
                          </Typography>
                          <Typography variant="h6">4</Typography>
                        </div>
                      </div>
                    ))}
                  </Typography>
                  {auth && (
                    <div>
                      <IconButton
                        size="large"
                        aria-label="account of current user"
                        aria-controls="menu-appbar"
                        aria-haspopup="true"
                        onClick={handleMenu}
                        color="inherit"
                      >
                        <MoreIcon />
                      </IconButton>
                      <Menu
                        id="menu-appbar"
                        anchorEl={anchorEl}
                        anchorOrigin={{
                          vertical: 'top',
                          horizontal: 'right',
                        }}
                        keepMounted
                        transformOrigin={{
                          vertical: 'top',
                          horizontal: 'right',
                        }}
                        open={Boolean(anchorEl)}
                        onClose={handleClose}
                      >
                        <Button onClick={handleClickOpen}>
                          <MenuItem onClick={handleClose}>Edit Profile</MenuItem>
                        </Button>
                      </Menu>
                      <div>
                        <Dialog
                          open={open}
                          TransitionComponent={Transition}
                          keepMounted
                          onClose={handleCloseDialog}
                          aria-describedby="alert-dialog-slide-description"
                        >
                          <DialogTitle>Edit Profil</DialogTitle>
                          <DialogContent>
                            <DialogContentText id="alert-dialog-slide-description">
                              <EditProfil data={data} />
                            </DialogContentText>
                          </DialogContent>
                          <DialogActions>
                            <Button variant="contained" onClick={handleCloseDialog}>
                              Tutup
                            </Button>
                            <Button disabled variant="contained" onClick={handleCloseDialog}>
                              Simpan
                            </Button>
                          </DialogActions>
                        </Dialog>
                      </div>
                    </div>
                  )}
                </StyledToolbar>
              </AppBar>
            </Box>
          </div>
        </div>
        <div className="col-span-2">
          <div> </div>
        </div>
        <div>
          <div> </div>
        </div>
      </div>
    </div>
  );
}
