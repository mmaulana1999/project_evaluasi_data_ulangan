import * as React from 'react';
import Slide from '@mui/material/Slide';
import { TextField } from '@mui/material';

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default function EditProfil({ data }) {
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <div className="flex justify-evenly">
        <div className="m-10">
          <TextField id="outlined-basic" label="Nama" variant="outlined" value={data?.nama} />
        </div>
        <div className="m-10">
          <TextField id="outlined-basic" label="Email" variant="outlined" value={data?.email} />
        </div>
      </div>
    </div>
  );
}
