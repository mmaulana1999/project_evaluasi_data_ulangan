/* eslint-disable no-alert */
import * as React from 'react';
import PropTypes from 'prop-types';
import SwipeableViews from 'react-swipeable-views';
import { useTheme } from '@mui/material/styles';
import AppBar from '@mui/material/AppBar';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import { useEffect, useRef, useState } from 'react';
import { CardContent } from '@mui/material';
import PilihanGanda from './PilihanGanda';
import Essay from './Essay';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `full-width-tab-${index}`,
    'aria-controls': `full-width-tabpanel-${index}`,
  };
}

export default function SoalMataPelajaranPage() {
  const theme = useTheme();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeIndex = (index) => {
    setValue(index);
  };

  // start Time

  const Ref = useRef(null);

  const [timer, setTimer] = useState('00:00:00');
  const [timerEnd, setTimerEnd] = useState('00:00:00');
  const [dataMapel, setDataMapel] = useState('');

  const getTimeRemaining = (e) => {
    const total = Date.parse(e) - Date.parse(new Date());
    const seconds = Math.floor((total / 1000) % 60);
    const minutes = Math.floor((total / 1000 / 60) % 60);
    const hours = Math.floor((total / 1000 / 60 / 60) % 24);
    return {
      total,
      hours,
      minutes,
      seconds,
    };
  };

  const startTimer = (e) => {
    const { total, hours, minutes, seconds } = getTimeRemaining(e);
    if (total >= 0) {
      setTimer(
        `${hours > 9 ? hours : `0${hours}`}:${minutes > 9 ? minutes : `0${minutes}`}:${
          seconds > 9 ? seconds : `0${seconds}`
        }`
      );
      setTimerEnd(
        `${hours > 9 ? hours : `0${hours}`}:${minutes > 9 ? minutes : `0${minutes}`}:${
          seconds > 9 ? seconds : `0${seconds}`
        }`
      );
    }
  };

  const clearTimer = (e) => {
    setTimer('00:0:10');
    if (Ref.current) clearInterval(Ref.current);
    const id = setInterval(() => {
      startTimer(e);
    }, 1000);
    Ref.current = id;
    clearInterval(Ref.current);
  };
  const clearTimerEnd = (e) => {
    setTimer('00:0:10');
    if (Ref.current) clearInterval(Ref.current);
    const id = setInterval(() => {
      startTimer(e);
    }, 1000);
    Ref.current = id;
  };

  const getDeadTime = () => {
    const deadline = new Date();
    deadline.setSeconds(deadline.getSeconds() + 3600);
    return deadline;
  };
  // const getDeadTimeEnd = () => {
  //   const deadline = new Date();
  //   deadline.setSeconds(deadline.getSeconds() + 20);
  //   return deadline;
  // };

  useEffect(() => {
    clearTimer(getDeadTime());
    // clearTimerEnd(getDeadTimeEnd());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  // const onClickReset = () => {
  //   clearTimer(getDeadTime());
  //   clearTimerEnd(getDeadTime());
  // };

  const Test = () => {
    alert('Test1 BERHASIL');
  };
  // End Time

  return (
    <div>
      <Box className="w-224" sx={{ display: 'flex', flexDirection: 'column' }}>
        <CardContent sx={{ flex: '1 0 auto' }}>
          {timer === '00:00:00' ? (
            <Typography component="div" variant="h5">
              Berakhir
            </Typography>
          ) : (
            <Typography component="div" variant="h5">
              Waktu Berakhir
            </Typography>
          )}
          {timer === '00:00:00' ? (
            <Typography component="div" variant="subtitle2">
              --:--:--
            </Typography>
          ) : (
            <Typography component="div" variant="subtitle2">
              Sebelum {timer}
            </Typography>
          )}
          {/* <Typography
                                variant="subtitle1"
                                color="text.secondary"
                                component="div"
                              >
                                {item?.maPel}
                              </Typography> */}
        </CardContent>
      </Box>
      <div className="w-full flex justify-center">
        <Typography variant="h4">LEMBAR SOAL</Typography>
      </div>

      <Box sx={{ bgcolor: 'background.paper', width: '100%' }}>
        <AppBar position="static">
          <Tabs
            value={value}
            onChange={handleChange}
            indicatorColor="secondary"
            textColor="inherit"
            variant="fullWidth"
            aria-label="full width tabs example"
          >
            <Tab label="Pilihan Ganda" {...a11yProps(0)} />
            <Tab label="Essay" {...a11yProps(1)} />
            {/* <Tab label="Status" {...a11yProps(2)} /> */}
          </Tabs>
        </AppBar>
        <SwipeableViews
          axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
          index={value}
          onChangeIndex={handleChangeIndex}
        >
          <TabPanel value={value} index={0} dir={theme.direction}>
            <PilihanGanda />
          </TabPanel>
          <TabPanel value={value} index={1} dir={theme.direction}>
            <Essay />
          </TabPanel>
          {/* <TabPanel value={value} index={2} dir={theme.direction}>
          Status
        </TabPanel> */}
        </SwipeableViews>
      </Box>
    </div>
  );
}
