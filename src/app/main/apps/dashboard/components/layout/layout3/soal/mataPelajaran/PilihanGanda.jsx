/* eslint-disable no-nested-ternary */
/* eslint-disable no-const-assign */
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Checkbox from '@mui/material/Checkbox';
import { Alert, Button, Snackbar, Typography } from '@mui/material';
import { useState, useEffect } from 'react';
import axios from 'axios';
import FuseLoading from '@fuse/core/FuseLoading';
import Loading from './Loading';

export default function PilihanGanda() {
  const [checked, setChecked] = useState([0]);
  const [nilai, setNilai] = useState(false);
  const [open, setOpen] = useState(false);

  const handleToggle = (value) => () => {
    const currentIndex = checked.indexOf(value);
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    setChecked(newChecked);
  };
  const handleChange = (event) => {
    setChecked(event.target.checked);
  };
  const handleTest = () => {
    return <Loading />;
  };

  const handleClick = () => {
    setOpen(true);
  };

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    setOpen(false);
  };
  const [data, setData] = useState();
  const [loading, setLoading] = useState(false);
  const getData = async () => {
    setLoading(false);
    const response = await axios
      .get(`${process.env.REACT_APP_API_URL_SOAL_DUMMY_PILIHAN_GANDA}`)
      .then((res) => {
        console.log(res, 'dummy');
        setData(res.data);
        setLoading(true);
      })
      .catch((err) => {
        console.log(err);
        setLoading(true);
      });
    setLoading(true);
  };
  useEffect(() => {
    getData();
  }, []);

  return (
    <List sx={{ width: '100%', maxWidth: '100%', bgcolor: 'background.paper' }}>
      {loading !== true ? (
        <div>
          <FuseLoading />
        </div>
      ) : data.length !== 0 ? (
        <div>
          {data?.map((item, index) => {
            const labelId = `checkbox-list-label-${item?.id}`;

            return (
              <div>
                <div className="flex">
                  <Typography variant="h6">{index + 1})</Typography>
                  <Typography variant="h6">. {item?.soalPG}</Typography>
                </div>
                <ListItem key={item.id} disablePadding>
                  <div className="block">
                    <ListItemButton role={undefined} onClick={handleToggle(item.id)} dense>
                      <ListItemIcon>
                        <Checkbox
                          edge="start"
                          checked={checked.indexOf(item.id) !== -1}
                          tabIndex={-1}
                          disableRipple
                          inputProps={{ 'aria-labelledby': labelId }}
                          // onChange={(e) => setNilai(e.target.checked)}
                        />
                      </ListItemIcon>
                      <div className="">
                        <ListItemText id={labelId} primary={`A. ${item?.pilihanA}`} />
                      </div>
                    </ListItemButton>
                    <ListItemButton role={undefined} onClick={handleToggle(item.id)} dense>
                      <ListItemIcon>
                        <Checkbox
                          edge="start"
                          checked={checked.indexOf(item.id) !== -1}
                          tabIndex={-1}
                          disableRipple
                          inputProps={{ 'aria-labelledby': labelId }}
                        />
                      </ListItemIcon>
                      <div className="">
                        <ListItemText id={labelId} primary={`B. ${item?.pilihanB}`} />
                      </div>
                    </ListItemButton>
                    <ListItemButton role={undefined} onClick={handleToggle(item.id)} dense>
                      <ListItemIcon>
                        <Checkbox
                          edge="start"
                          checked={checked.indexOf(item.id) !== -1}
                          tabIndex={-1}
                          disableRipple
                          inputProps={{ 'aria-labelledby': labelId }}
                        />
                      </ListItemIcon>
                      <div className="">
                        <ListItemText id={labelId} primary={`C. ${item?.pilihanC}`} />
                      </div>
                    </ListItemButton>
                    <ListItemButton role={undefined} onClick={handleToggle(item.id)} dense>
                      <ListItemIcon>
                        <Checkbox
                          edge="start"
                          checked={checked.indexOf(item.id) !== -1}
                          tabIndex={-1}
                          disableRipple
                          inputProps={{ 'aria-labelledby': labelId }}
                        />
                      </ListItemIcon>
                      <div className="">
                        <ListItemText id={labelId} primary={`D. ${item?.pilihanD}`} />
                      </div>
                    </ListItemButton>
                    <ListItemButton role={undefined} onClick={handleToggle(item.id)} dense>
                      <ListItemIcon>
                        <Checkbox
                          edge="start"
                          checked={checked.indexOf(item.id) !== -1}
                          tabIndex={-1}
                          disableRipple
                          inputProps={{ 'aria-labelledby': labelId }}
                        />
                      </ListItemIcon>
                      <div className="">
                        <ListItemText id={labelId} primary={`E. ${item?.pilihanE}`} />
                      </div>
                    </ListItemButton>
                  </div>
                </ListItem>
              </div>
            );
          })}
        </div>
      ) : (
        <Alert variant="outlined" severity="warning">
          Belum Ada Soal!
        </Alert>
      )}
      <div className="m-10 flex justify-end">
        <Button onClick={handleClick} variant="contained">
          Kirim
        </Button>
        <Snackbar open={open} autoHideDuration={2000} onClose={handleClose}>
          <Alert onClose={handleClose} severity="success" sx={{ width: '100%' }}>
            Data telah dikirim.
          </Alert>
        </Snackbar>
      </div>
    </List>
  );
}
