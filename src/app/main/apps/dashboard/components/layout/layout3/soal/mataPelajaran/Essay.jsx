/* eslint-disable no-alert */
/* eslint-disable no-nested-ternary */
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import { Alert, Button, TextField, Typography } from '@mui/material';
import * as React from 'react';
import { useFormControl } from '@mui/material/FormControl';
import OutlinedInput from '@mui/material/OutlinedInput';
import Box from '@mui/material/Box';
import FormHelperText from '@mui/material/FormHelperText';
import { useState, useEffect } from 'react';
import axios from 'axios';
import FuseLoading from '@fuse/core/FuseLoading';
import { toast } from 'react-toastify';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';

function MyFormHelperText() {
  const { focused } = useFormControl() || {};

  const helperText = React.useMemo(() => {
    if (focused) {
      return 'This field is being focused';
    }

    return 'Helper text';
  }, [focused]);

  return <FormHelperText>{helperText}</FormHelperText>;
}

export default function Essay() {
  const [data, setData] = useState();
  const [dataCatatan, setDataCatatan] = useState();
  const [loading, setloading] = useState(false);
  const [loadingJawaban, setloadingJawaban] = useState(false);
  const [JawabanEssay1, setJawabanEssay1] = useState('');
  const [JawabanEssay2, setJawabanEssay2] = useState('');
  const [JawabanEssay3, setJawabanEssay3] = useState('');
  const [JawabanEssay4, setJawabanEssay4] = useState('');
  const [JawabanEssay5, setJawabanEssay5] = useState('');

  const [open, setOpen] = useState(false);
  const handleClearStateJawaban = () => {
    setJawabanEssay1('');
    setJawabanEssay2('');
    setJawabanEssay3('');
    setJawabanEssay4('');
    setJawabanEssay5('');
  };

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const getData = async () => {
    setloading(true);
    const response = await axios
      .get(`${process.env.REACT_APP_API_URL_SOAL_DUMMY_ESSAY}`)
      .then((res) => {
        console.log(res, 'essaySoal');
        setData(res.data);
        setloading(false);
      })
      .catch((err) => {
        console.log(err);
        setloading(false);
      });
    setloading(false);
  };
  const getDataJawaban = async () => {
    setloadingJawaban(true);
    const response = await axios
      .get(`https://633fa89dd1fcddf69ca6ec51.mockapi.io/jabawan/v1/jawabanEssay`)
      .then((res) => {
        setloadingJawaban(false);
        console.log(res, 'getDataJawaban');
        // setDataJawaban(res.data);
      })
      .catch((err) => {
        console.log(err);
        setloadingJawaban(false);
      });
    setloadingJawaban(false);
  };
  const getDataCatatan = async () => {
    setloadingJawaban(true);
    const response = await axios
      .get(`https://633cf7147e19b1782904f3cc.mockapi.io/soalDummy/v1/catatan`)
      .then((res) => {
        setloadingJawaban(false);
        console.log(res, 'getDataJawaban');
        setDataCatatan(res.data);
      })
      .catch((err) => {
        console.log(err);
        setloadingJawaban(false);
      });
    setloadingJawaban(false);
  };

  const HandleSubmit = async (event) => {
    setloadingJawaban(true);
    event.preventDefault();
    const response = await axios
      .post(`https://633fa89dd1fcddf69ca6ec51.mockapi.io/jabawan/v1/jawabanEssay`, {
        JawabanEssay1,
        JawabanEssay2,
        JawabanEssay3,
        JawabanEssay4,
        JawabanEssay5,
      })
      .then((res) => {
        setloadingJawaban(false);
        console.log(res, 'TambahJawabanEssay');
        getDataJawaban();
        alert('Jawaban Behasil Di Kirim');
        toast.success('Jawaban Behasil Di Kirim');
        handleClearStateJawaban();
        // getData();
      })
      .catch((err) => {
        console.log(err, 'err');
      });
  };

  useEffect(() => {
    getData();
    getDataJawaban();
    getDataCatatan();
  }, []);

  return (
    <List sx={{ width: '100%', maxWidth: '100%', bgcolor: 'background.paper' }}>
      {loading === true ? (
        <div>
          <FuseLoading />
        </div>
      ) : data?.length !== 0 ? (
        <div>
          <div>
            <div className="flex justify-end m-10">
              <Button variant="contained" onClick={handleClickOpen}>
                Catatan!
              </Button>
            </div>
            <Dialog
              open={open}
              onClose={handleClose}
              aria-labelledby="alert-dialog-title"
              aria-describedby="alert-dialog-description"
            >
              <DialogTitle id="alert-dialog-title">Perhatian!</DialogTitle>
              <DialogContent>
                {dataCatatan?.map((i) => (
                  <DialogContentText id="alert-dialog-description">{i?.catatan}</DialogContentText>
                ))}
              </DialogContent>
              <DialogActions>
                <Button onClick={handleClose} autoFocus>
                  Tutup
                </Button>
              </DialogActions>
            </Dialog>
          </div>
          <div>
            {data?.map((item, index) => (
              <ListItem className="block" key={item} disableGutters>
                <div className="flex">
                  <Typography variant="h6">{index + 1}</Typography>
                  <Typography variant="h6">. {item?.buatEssay}</Typography>
                  {/* <ListItemText primary={`1. ${value?.soalPG}`} />  */}
                </div>
              </ListItem>
            ))}
          </div>
          <div className="flex justify-center m-10">
            <Typography variant="h6">Lembar Jawaban</Typography>
          </div>
          <div>
            <div>
              {loadingJawaban === true ? (
                <div>
                  <FuseLoading />
                </div>
              ) : (
                <div>
                  <div className="m-10 block">
                    <Box component="form" noValidate autoComplete="off">
                      {/* <Typography variant="h6">{index + 1}).</Typography> */}
                      <div className="m-10">
                        <TextField
                          value={JawabanEssay1}
                          onChange={(e) => setJawabanEssay1(e.target.value)}
                          fullWidth
                          multiline
                          rows={4}
                          placeholder="Jawaban"
                          label="Tulis Jawaban Disini 1"
                        >
                          <OutlinedInput placeholder="Please enter text" />
                          <MyFormHelperText />
                        </TextField>
                      </div>
                      <div className="m-10">
                        <TextField
                          value={JawabanEssay2}
                          onChange={(e) => setJawabanEssay2(e.target.value)}
                          fullWidth
                          multiline
                          rows={4}
                          placeholder="Jawaban"
                          label="Tulis Jawaban Disini 2"
                        >
                          <OutlinedInput placeholder="Please enter text" />
                          <MyFormHelperText />
                        </TextField>
                      </div>
                      <div className="m-10">
                        <TextField
                          value={JawabanEssay3}
                          onChange={(e) => setJawabanEssay3(e.target.value)}
                          fullWidth
                          multiline
                          rows={4}
                          placeholder="Jawaban"
                          label="Tulis Jawaban Disini 3"
                        >
                          <OutlinedInput placeholder="Please enter text" />
                          <MyFormHelperText />
                        </TextField>
                      </div>
                      <div className="m-10">
                        <TextField
                          value={JawabanEssay4}
                          onChange={(e) => setJawabanEssay4(e.target.value)}
                          fullWidth
                          multiline
                          rows={4}
                          placeholder="Jawaban"
                          label="Tulis Jawaban Disini 4"
                        >
                          <OutlinedInput placeholder="Please enter text" />
                          <MyFormHelperText />
                        </TextField>
                      </div>
                      <div className="m-10">
                        <TextField
                          value={JawabanEssay5}
                          onChange={(e) => setJawabanEssay5(e.target.value)}
                          fullWidth
                          multiline
                          rows={4}
                          placeholder="Jawaban"
                          label="Tulis Jawaban Disini 5"
                        >
                          <OutlinedInput placeholder="Please enter text" />
                          <MyFormHelperText />
                        </TextField>
                      </div>
                    </Box>
                  </div>
                </div>
              )}
            </div>
          </div>
        </div>
      ) : (
        <Alert variant="outlined" severity="warning">
          Belum Ada Soal!
        </Alert>
      )}
      <div className="m-10 flex justify-end">
        <Button
          onClick={HandleSubmit}
          disabled={
            JawabanEssay1 === '' ||
            JawabanEssay2 === '' ||
            JawabanEssay3 === '' ||
            JawabanEssay4 === '' ||
            JawabanEssay5 === ''
          }
          variant="contained"
        >
          Kirim
        </Button>
      </div>
    </List>
  );
}
