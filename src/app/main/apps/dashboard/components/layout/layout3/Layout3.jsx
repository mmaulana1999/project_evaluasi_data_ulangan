/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable no-alert */
/* eslint-disable react-hooks/exhaustive-deps */
import { Alert, Button, Divider } from '@mui/material';
import { useState, useRef, useEffect, createContext } from 'react';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import { useTheme } from '@mui/material/styles';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { Link } from 'react-router-dom';
import yosemite from '../../fakeData/dataImg/fall-glow.jpg';
import MataPelajaran from '../../fakeData/MataPelajaran';
import FuseAnimate from '@fuse/core/FuseAnimate';

export const UserContext = createContext();

export default function Layout3() {
  // timer
  const Ref = useRef(null);

  const [timer, setTimer] = useState('00:00:00');
  const [timerEnd, setTimerEnd] = useState('00:00:00');
  const [dataMapel, setDataMapel] = useState('');

  const getTimeRemaining = (e) => {
    const total = Date.parse(e) - Date.parse(new Date());
    const seconds = Math.floor((total / 1000) % 60);
    const minutes = Math.floor((total / 1000 / 60) % 60);
    const hours = Math.floor((total / 1000 / 60 / 60) % 24);
    return {
      total,
      hours,
      minutes,
      seconds,
    };
  };

  const startTimer = (e) => {
    const { total, hours, minutes, seconds } = getTimeRemaining(e);
    if (total >= 0) {
      setTimer(
        `${hours > 9 ? hours : `0${hours}`}:${minutes > 9 ? minutes : `0${minutes}`}:${seconds > 9 ? seconds : `0${seconds}`
        }`
      );
      setTimerEnd(
        `${hours > 9 ? hours : `0${hours}`}:${minutes > 9 ? minutes : `0${minutes}`}:${seconds > 9 ? seconds : `0${seconds}`
        }`
      );
    }
  };

  const clearTimer = (e) => {
    setTimer('00:0:10');
    if (Ref.current) clearInterval(Ref.current);
    const id = setInterval(() => {
      startTimer(e);
    }, 1000);
    Ref.current = id;
    // clearInterval(Ref.current);
  };
  const clearTimerEnd = (e) => {
    setTimer('00:0:10');
    if (Ref.current) clearInterval(Ref.current);
    const id = setInterval(() => {
      startTimer(e);
    }, 1000);
    Ref.current = id;
  };

  const getDeadTime = () => {
    const deadline = new Date();
    deadline.setSeconds(deadline.getSeconds() + 3);
    return deadline;
  };
  const getDeadTimeEnd = () => {
    const deadline = new Date();
    deadline.setSeconds(deadline.getSeconds() + 20);
    return deadline;
  };

  useEffect(() => {
    clearTimer(getDeadTime());
    // clearTimerEnd(getDeadTimeEnd());
  }, []);
  const onClickReset = () => {
    clearTimer(getDeadTime());
    clearTimerEnd(getDeadTime());
  };

  const Test = () => {
    // alert('Test1 BERHASIL');
  };

  // end timer
  const theme = useTheme();
  return (
    <UserContext.Provider value={dataMapel}>
      <Box
        sx={{
          margin: '10px',
          display: 'flex',
          flexWrap: 'wrap',
          '& > :not(style)': {
            m: 1,
            width: 2000,
            // height: 128,
          },
        }}
      >
        <Paper className="h-full" elevation={3}>
          <div>
            <div className="flex justify-between">
              <FuseAnimate animation="transition.slideLeftIn" delay={1000}>
                <div className="m-20 text-2xl">Pelajaran</div>
              </FuseAnimate>
              <div className="m-20">
                <FuseAnimate animation="transition.slideLeftIn" delay={1300}>
                  <div>
                    <Button className="ml-5 mr-5" variant="contained">
                      Test 1
                    </Button>
                    <Button className="ml-5 mr-5" variant="contained">
                      Test 2
                    </Button>
                  </div>
                </FuseAnimate>
              </div>
            </div>
            <div className="m-10">
              <FuseAnimate animation="transition.slideLeftIn" delay={1300}>
                <Alert severity="info">
                  Berikan Saran Untuk Kami WEB developer Sebagai Pengembang!
                </Alert>
              </FuseAnimate>
            </div>
            {/* <Link to="/soalPage"> */}
            <FuseAnimate animation="transition.slideLeftIn" delay={1500}>
              <div className="m-10">
                <Button variant="contained" onClick={onClickReset}>
                  Test Timer
                </Button>
              </div>
            </FuseAnimate>
            {/* </Link> */}
            <Divider />
            <div>
              <FuseAnimate animation="transition.expandIn" delay={1300}>
                <div className="w-full flex justify-evenly flex-wrap">
                  {MataPelajaran.map((item) => (
                        <div className="m-10 ">
                          <Card sx={{ display: 'flex', width: '100%' }}>
                            <Button disabled={timer !== '00:00:00'} onClick={Test}>
                              <Link to="/dashboard/soalPage">
                                <CardMedia
                                  component="img"
                                  sx={{ width: 151 }}
                                  image={yosemite}
                                  alt="Mata Pelajaran"
                                />
                              </Link>
                            </Button>
                            <Box className="w-224" sx={{ display: 'flex', flexDirection: 'column' }}>
                            <FuseAnimate animation="transition.slideLeftIn" delay={1700}>
                              <CardContent sx={{ flex: '1 0 auto' }}>
                                {timer === '00:00:00' ? (
                                  <Typography component="div" variant="h5">
                                    Dimulai
                                  </Typography>
                                ) : (
                                  <Typography component="div" variant="h5">
                                    Belum Dimulai
                                  </Typography>
                                )}
                                {timer === '00:00:00' ? (
                                  <Typography component="div" variant="subtitle2">
                                    Dimulai --:--:--
                                  </Typography>
                                ) : (
                                  <Typography component="div" variant="subtitle2">
                                    Dimulai {timer}
                                  </Typography>
                                )}
                                <Typography
                                  variant="subtitle1"
                                  color="text.secondary"
                                  component="div"
                                >
                                  {item?.maPel}
                                </Typography>
                              </CardContent>
                              </FuseAnimate>
                            </Box>
                          </Card>
                          <div className="m-10">
                            <div>
                              <FuseAnimate animation="transition.slideLeftIn" delay={1500}>
                                <ul>
                                  <li>Nama Guru : {item?.namaGuru} </li>
                                  <li>Kelas : {item?.Kelas} </li>
                                </ul>
                              </FuseAnimate>
                            </div>
                          </div>
                        </div>

                  ))}
                </div>
              </FuseAnimate>
            </div>
          </div>
        </Paper>
      </Box>
    </UserContext.Provider>
  );
}
