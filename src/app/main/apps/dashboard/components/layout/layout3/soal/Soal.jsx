/* eslint-disable react-hooks/rules-of-hooks */
import { Button, Typography } from '@mui/material';
// import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import SchoolIcon from '@mui/icons-material/School';
import { Link } from 'react-router-dom';
import { useState, useEffect } from 'react';
import axios from 'axios';

/* eslint-disable import/prefer-default-export */
export const Soal = () => {
  const [open, setOpen] = useState(false);
  const [data, setData] = useState();

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const getData = async () => {
    const response = await axios
      .get(`${process.env.REACT_APP_API_URL_SOAL_DUMMY_CATATAN}`)
      .then((res) => {
        console.log(res, 'dummy catatan');
        setData(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  useEffect(() => {
    getData();
  }, []);
  return (
    <div>
      <Typography variant="h4">Soal Ulangan Matematika</Typography>
      <div className="w-1/3 flex justify-between mt-10 mb-10">
        <div>
          <SchoolIcon fontSize="large" />
        </div>
        <Typography variant="h5">Silahkan Kerjakan Soal Yang Di Sediakan</Typography>
      </div>
      <div className=" mt-10 mb-10">
        <div className="flex justify-center items-center">
          <Typography variant="subtitle1">Total Soal 30</Typography>
        </div>
        <div className="flex justify-center items-center">
          <Typography variant="subtitle1">Waktu 60 Menit</Typography>
        </div>
      </div>
      <div className=" mt-10 mb-10">
        <Button onClick={handleClickOpen} variant="contained">
          Mulai Sekarang
        </Button>
      </div>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">Catatan Dari Guru.</DialogTitle>
        <DialogContent>
          {data?.map((item, index) => (
            <DialogContentText className="mb-10" id="alert-dialog-description">
              {index + 1}). {item?.catatan}
            </DialogContentText>
          ))}
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Batal</Button>
          <Link to="/dashboard/soalPage/soalMataPelajaran">
            <Button autoFocus>Mulai</Button>
          </Link>
        </DialogActions>
      </Dialog>
    </div>
  );
};
