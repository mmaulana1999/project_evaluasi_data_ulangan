/* eslint-disable import/prefer-default-export */
import FusePageSimple from '@fuse/core/FusePageSimple';
import { Typography } from '@mui/material';
import { styled } from '@mui/styles';
import { Link } from 'react-router-dom';
import ReplyAllIcon from '@mui/icons-material/ReplyAll';
import SoalMataPelajaranPage from './SoalMataPelajaranPage';
// import { SoalMataPelajaranPage } from './SoalMataPelajaranPage';

const Root = styled(FusePageSimple)({
  '& .FusePageSimple-header': {},
  '& .FusePageSimple-toolbar': {},
  '& .FusePageSimple-content': {},
  '& .FusePageSimple-sidebarHeader': {},
  '& .FusePageSimple-sidebarContent': {},
});

export const SoalMataPelajaran = () => {
  return (
    <div>
      <div>
        <Root
          header={
            <div className="w-full h-4/5 flex items-center">
              <div className="flex justify-start ">
                <Link to="/dashboard/soalPage">
                  <Typography variant="h6">
                    <ReplyAllIcon fontSize="large" />
                  </Typography>
                </Link>
              </div>
              <div className="w-full h-2/6">
                <div className="flex justify-center">
                  <Typography variant="h4">Muhamad Maulana</Typography>
                </div>
                <div className="flex justify-center">
                  <Typography variant="h6">1209387</Typography>
                </div>
              </div>
            </div>
          }
          contentToolbar={
            <div className="px-24 mt-10">
              <div>
                <Typography variant="h5">Ulangan Akhir Semester 2 </Typography>
                <Typography variant="h5">TAHUN AJARAN 2022/2023 </Typography>
              </div>
              {/* <div>
                <table>
                  <tr>
                    <td>Mata Peajaran</td>
                    <td>Ipa</td>
                  </tr>
                  <tr>
                    <td>Kelas</td>
                    <td>X TKJ</td>
                  </tr>
                  <tr>
                    <td>Hari/Tanggal</td>
                    <td>07 JUNI 2022</td>
                  </tr>
                  <tr>
                    <td>Waktu/jam</td>
                    <td>07:30 - 09: 30</td>
                  </tr>
                </table>
              </div> */}
            </div>
          }
          content={
            <div className="p-24">
              <SoalMataPelajaranPage />
            </div>
          }
        />
      </div>
    </div>
  );
};
