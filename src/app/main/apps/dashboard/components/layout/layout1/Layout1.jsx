import { Alert, AlertTitle } from '@mui/material';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import { Link } from 'react-router-dom';
import Header from '../../fakeData/Header';
import FuseAnimate from '@fuse/core/FuseAnimate';

export default function Layout1() {
  return (
    <div className="m-20">
      <FuseAnimate animation="transition.expandIn" delay={1000}>
        <Alert severity="success">
          <FuseAnimate animation="transition.expandIn" delay={1300}>
            <AlertTitle>Login Success</AlertTitle>
          </FuseAnimate>
          <FuseAnimate animation="transition.expandIn" delay={1300}>
            <div>
              Muhamad Maulana — <strong>123456789</strong>
            </div>
          </FuseAnimate>
        </Alert>
      </FuseAnimate>
      <div className="flex flex-wrap mt-20 justify-between">
        {Header.map((item) => (
          <div className="">
            <Card className="flex mb-10" sx={{ minWidth: 275 }}>
              <div className="flex-none w-224 h-136">
                <CardContent>
                  <FuseAnimate animation="transition.slideLeftIn" delay={700}>
                    <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
                      {item?.labels}
                    </Typography>
                  </FuseAnimate>
                  <FuseAnimate animation="transition.slideLeftIn" delay={700}>
                    <Typography variant="h5" component="div">
                      {item?.label}
                    </Typography>
                  </FuseAnimate>
                </CardContent>
                <FuseAnimate animation="transition.expandIn" delay={1300}>
                  <CardActions>
                    <Button size="small">Selanjutnya</Button>
                  </CardActions>
                </FuseAnimate>
              </div>
              <Link to={item?.path}>
                <FuseAnimate animation="transition.expandIn" delay={1500}>
                  <div className="grow h-14">
                    <CardActions>{item?.icon}</CardActions>
                  </div>
                </FuseAnimate>
              </Link>
            </Card>
          </div>
        ))}
      </div>
    </div>
  );
}
