import FusePageSimple from '@fuse/core/FusePageSimple';
import { Typography } from '@mui/material';
import { styled } from '@mui/styles';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import ReplyAllIcon from '@mui/icons-material/ReplyAll';
import ContentGradePage from './ContentGradePage';

/* eslint-disable import/prefer-default-export */
const Root = styled(FusePageSimple)({
  '& .FusePageSimple-header': {},
  '& .FusePageSimple-toolbar': {},
  '& .FusePageSimple-content': {},
  '& .FusePageSimple-sidebarHeader': {},
  '& .FusePageSimple-sidebarContent': {},
});
export const GradePage = () => {
  const { t } = useTranslation('examplePage');
  return (
    <Root
      header={
        <div className="w-full h-4/5 flex items-center">
          <div className="flex justify-start ">
            <Link to="/dashboard">
              <Typography variant="h6">
                <ReplyAllIcon fontSize="large" />
              </Typography>
            </Link>
          </div>
          <div className="w-full h-2/6">
            <div className="flex justify-center">
              <Typography variant="h4">Muhamad Maulana</Typography>
            </div>
            <div className="flex justify-center">
              <Typography variant="h6">1209387</Typography>
            </div>
          </div>
        </div>
      }
      contentToolbar={
        <div className="px-24">
          <Typography variant="h5">Pelajaran Yang Sudah Di Selesaikan</Typography>
        </div>
      }
      content={
        <div className="p-24">
          <Typography>Content</Typography>
          {/* <ContentGradePageHasil /> */}
          <ContentGradePage />
          <br />
        </div>
      }
    />
  );
};
