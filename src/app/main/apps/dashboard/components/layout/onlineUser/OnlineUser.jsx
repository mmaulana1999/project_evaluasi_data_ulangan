/* eslint-disable import/no-extraneous-dependencies */
import { Divider } from '@mui/material';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import axios from 'axios';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import { useState, useEffect } from 'react';
import moment from 'moment';
import FuseLoading from '@fuse/core/FuseLoading';
import FuseAnimate from '@fuse/core/FuseAnimate';

export default function OnlineUser() {
  const [tanggal, setTanggal] = useState(moment().format('DD-MM-YYYY'));
  const [data, setData] = useState();
  const url = 'https://jsonplaceholder.typicode.com/users';
  useEffect(() => {
    axios
      .get(url)
      .then((res) => {
        console.log(res);
        setData(res?.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  return (
    <Box
      sx={{
        margin: '10px',
        display: 'flex',
        flexWrap: 'wrap',
        '& > :not(style)': {
          m: 1,
          width: 2000,
        },
      }}
    >
      <Paper className="h-full" elevation={3}>
        {data?.length === '' ? (
          <FuseLoading />
        ) : (
          <div>
            <FuseAnimate animation="transition.slideLeftIn" delay={1000}>
              <div className="text-2xl m-20">Online User</div>
            </FuseAnimate>
            <Divider />
            <FuseAnimate animation="transition.slideLeftIn" delay={1300}>
              <div className="ml-20 mt-20">
                {data?.length * 2} Users Online Tanggal {tanggal}
              </div>
            </FuseAnimate>
            <div className="mb-20">
              {data?.map((item) => (
                <div
                  className="ml-20 mt-10 flex text-base
            "
                >
                  <FuseAnimate animation="transition.expandIn" delay={1000}>
                    <div className="mr-5">
                      <AccountCircleIcon />
                    </div>
                  </FuseAnimate>
                  <div>
                    {item?.name} - {item?.address?.zipcode}
                  </div>
                </div>
              ))}
              {data?.map((item) => (
                <div
                  className="ml-20 mt-10 flex text-base
            "
                >
                  <FuseAnimate animation="transition.expandIn" delay={1000}>
                    <div className="mr-5">
                      <AccountCircleIcon />
                    </div>
                  </FuseAnimate>
                  <div>
                    {item?.name} - {item?.address?.zipcode}
                  </div>
                </div>
              ))}
            </div>
          </div>
        )}
      </Paper>
    </Box>
  );
}
