import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import SecurityIcon from '@mui/icons-material/Security';
import ListAltIcon from '@mui/icons-material/ListAlt';
import MiscellaneousServicesIcon from '@mui/icons-material/MiscellaneousServices';
import SummarizeIcon from '@mui/icons-material/Summarize';
import LockOpenIcon from '@mui/icons-material/LockOpen';
import MobileFriendlyIcon from '@mui/icons-material/MobileFriendly';

const ProfilData = [
  {
    label: 'User details',
    labels: 'Pesan',
    path: '/dashboard/gradePage',
    icon: <AccountCircleIcon fontSize="large" />,
  },
  {
    label: 'Privacy and policies',
    labels: 'Profil Kamu',
    path: '/dashboard/profil',
    icon: <SecurityIcon fontSize="large" />,
  },
  {
    label: 'Course details',
    labels: 'Ubah Pengaturan',
    path: '/dashboard/settings',
    icon: <ListAltIcon fontSize="large" />,
  },
  {
    label: 'Miscellaneous',
    labels: 'Himpunan Nilai',
    path: '/dashboard/message',
    icon: <MiscellaneousServicesIcon fontSize="large" />,
  },
  {
    label: 'Reports',
    labels: 'Himpunan Nilai',
    path: '/dashboard/message',
    icon: <SummarizeIcon fontSize="large" />,
  },
  {
    label: 'Login activity',
    labels: 'Himpunan Nilai',
    path: '/dashboard/message',
    icon: <LockOpenIcon fontSize="large" />,
  },
  {
    label: 'Mobile app',
    labels: 'Himpunan Nilai',
    path: '/dashboard/message',
    icon: <MobileFriendlyIcon fontSize="large" />,
  },
];

export default ProfilData;
