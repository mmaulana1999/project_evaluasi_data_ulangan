import MessageIcon from '@mui/icons-material/Message';
import PermIdentityIcon from '@mui/icons-material/PermIdentity';
import GradingIcon from '@mui/icons-material/Grading';
import SettingsIcon from '@mui/icons-material/Settings';

const Header = [
  {
    label: 'Grades',
    labels: 'Pesan',
    path: '/dashboard/gradePage',
    icon: <GradingIcon fontSize="large" />,
  },
  {
    label: 'Profil',
    labels: 'Profil Kamu',
    path: '/dashboard/profil',
    icon: <PermIdentityIcon fontSize="large" />,
  },
  {
    label: 'Settings',
    labels: 'Ubah Pengaturan',
    path: '/dashboard/settings',
    icon: <SettingsIcon fontSize="large" />,
  },
  {
    label: 'Message',
    labels: 'Himpunan Nilai',
    path: '/dashboard/message',
    icon: <MessageIcon fontSize="large" />,
  },
];

export default Header;
