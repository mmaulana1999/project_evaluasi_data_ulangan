// import Layout1 from './layout/layout1/Layout1';
import Layout1 from './layout/layout1/Layout1';
import Layout3 from './layout/layout3/Layout3';
import OnlineUser from './layout/onlineUser/OnlineUser';

export default function DashboardContent() {
  return (
    <div className="grid grid-cols-3 gap-4">
      <div className="col-span-3">
        <Layout1 />
      </div>
      <div className="col-span-2">
        <Layout3 />
      </div>
      <div className="">
        <OnlineUser />
      </div>
    </div>
  );
}
