/* eslint-disable import/no-duplicates */
import i18next from 'i18next';
import en from '../../example/i18n/en';
import tr from '../../example/i18n/en';
import ar from '../../example/i18n/en';
import { GradePage } from './components/layout/layout1/grade/GradePage';
import { MessaeePage } from './components/layout/layout1/message/MessageePage';
import EditProfil from './components/layout/layout1/profil/editProfil/EditProfil';
import { ProfilPage } from './components/layout/layout1/profil/ProfilPage';
import { SettingsPage } from './components/layout/layout1/settings/SettingsPage';
import { SoalMataPelajaran } from './components/layout/layout3/soal/mataPelajaran/SoalMataPelajaran';
// import { SoalMataPelajaran } from './components/layout/layout3/soal/mataPelajaran/SoalMataPelajaran';
import { SoalPage } from './components/layout/layout3/soal/SoalPage';
import DashboardPage from './DashboardPage';

i18next.addResourceBundle('en', 'examplePage', en);
i18next.addResourceBundle('tr', 'examplePage', tr);
i18next.addResourceBundle('ar', 'examplePage', ar);

const DashboardConfig = {
  settings: {
    layout: {
      config: {},
    },
  },
  routes: [
    {
      path: 'dashboard',
      element: <DashboardPage />,
    },
    {
      path: 'dashboard/gradePage',
      element: <GradePage />,
    },
    {
      path: 'dashboard/profil',
      element: <ProfilPage />,
    },
    {
      path: 'dashboard/settings',
      element: <SettingsPage />,
    },
    {
      path: 'dashboard/message',
      element: <MessaeePage />,
    },
    {
      path: 'editProfil',
      element: <EditProfil />,
    },
    {
      path: 'dashboard/soalPage',
      element: <SoalPage />,
    },
    {
      path: 'dashboard/soalPage/soalMataPelajaran',
      element: <SoalMataPelajaran />,
    },
  ],
};

export default DashboardConfig;
