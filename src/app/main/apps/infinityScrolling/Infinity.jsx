/* eslint-disable no-shadow-restricted-names */
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import { useState } from 'react';
import InfinityPage from './components/InfinityPage';

export default function Infinity() {
  const [query, setQuery] = useState('');
  const [pageNumber, setpageNumber] = useState(1);
  InfinityPage(query, pageNumber);

  const handleSreach = (e) => {
    setQuery(e.target.value);
    setpageNumber(1);
  };
  return (
    <Box
      component="form"
      sx={{
        '& > :not(style)': { m: 1, width: '25ch' },
      }}
      noValidate
      autoComplete="off"
    >
      <TextField onChange={handleSreach} id="outlined-basic" label="Cari" variant="outlined" />
      {/* <InfinityPage /> */}
    </Box>
  );
}
