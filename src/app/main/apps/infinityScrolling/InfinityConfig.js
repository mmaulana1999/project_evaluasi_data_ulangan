/* eslint-disable import/no-duplicates */
import i18next from 'i18next';
import en from '../../example/i18n/en';
import tr from '../../example/i18n/en';
import ar from '../../example/i18n/en';
import Infinity from './Infinity';

i18next.addResourceBundle('en', 'examplePage', en);
i18next.addResourceBundle('tr', 'examplePage', tr);
i18next.addResourceBundle('ar', 'examplePage', ar);

const InfinityConfig = {
  settings: {
    layout: {
      config: {},
    },
  },
  routes: [
    {
      path: 'infinity',
      element: <Infinity />,
    },
  ],
};

export default InfinityConfig;
