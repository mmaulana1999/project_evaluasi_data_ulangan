/* eslint-disable no-return-assign */
/* eslint-disable no-useless-return */
import axios from 'axios';
import { useEffect } from 'react';

export default function InfinityPage(query, pageNumber) {
  useEffect(() => {
    let cancel;
    axios({
      method: 'GET',
      url: `http://openlibrary.org/search.json`,
      params: { q: query, page: pageNumber },
      cancelToken: new axios.CancelToken((c) => (cancel = c)),
    })
      .then((res) => {
        console.log(res.data, 'dataInfinity');
      })
      .catch((err) => {
        if (axios.isCancel(err)) return;
      });
    return () => cancel();
  }, [query, pageNumber]);

  return null;
}
