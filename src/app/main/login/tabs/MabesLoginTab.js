/* eslint-disable no-alert */
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';

import Typography from '@mui/material/Typography';

import { FormControl } from '@mui/material';
import axios from 'axios';
import { ToastContainer } from 'react-toastify';

import { useState } from 'react';

function MabesLoginTab() {
  const [userName, setUserName] = useState();
  const [password, setPasword] = useState();
  const [email, setEmail] = useState();

  const handleDaftar = () => {
    const data = {
      userName,
      password,
      email,
    };
    axios
      .post('http://localhost:8000/api/login/loginUser', data)
      .then((res) => {
        console.log(res, 'res login');
      })
      .catch((err) => {
        console.log(err, 'errlogin');
        alert('gagal login');
      });
  };
  return (
    <div className="w-full">
      <form
        className="flex flex-col justify-center w-full"
        // onSubmit={handleSubmit(onSubmit)}
        onSubmit={handleDaftar}
      >
        <FormControl fullWidth>
          <TextField
            className="mb-16"
            focused
            required
            // error={loginFiled === "" ? false : true}
            fullWidth
            labelId="username"
            id="standard-error-helper-text"
            label="Username"
            name="username"
            type="text"
            value={userName}
            onChange={(e) => setUserName(e.target.value)}
            // helperText={loginFiled}
          />
        </FormControl>
        <FormControl fullWidth>
          <TextField
            className="mb-16"
            focused
            required
            // error={loginFiled === "" ? false : true}
            fullWidth
            labelId="password"
            id="standard-error-helper-text"
            label="Password"
            name="password"
            type="password"
            value={password}
            onChange={(e) => setPasword(e.target.value)}
            // helperText={loginFiled}
          />
        </FormControl>
        <FormControl fullWidth>
          <TextField
            className="mb-16"
            focused
            required
            // error={loginFiled === "" ? false : true}
            fullWidth
            labelId="email"
            id="standard-error-helper-text"
            label="Email"
            name="email"
            type="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            // helperText={loginFiled}
          />
        </FormControl>
      </form>
      <Button
        type="submit"
        variant="contained"
        color="primary"
        className="w-full mx-auto mt-16"
        onClick={handleDaftar}
        aria-label="LOG IN"
        value="legacy"
      >
        Login
      </Button>

      <table className="w-full mt-32 text-center">
        <thead className="mb-4">
          <tr>
            <th>
              <Typography className="font-semibold text-11" color="textSecondary">
                Role
              </Typography>
            </th>
            <th>
              <Typography className="font-semibold text-11" color="textSecondary">
                Username
              </Typography>
            </th>
            <th>
              <Typography className="font-semibold text-11" color="textSecondary">
                Password
              </Typography>
            </th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <Typography className="font-medium text-11" color="textSecondary">
                Admin
              </Typography>
            </td>
            <td>
              <Typography className="text-11">mabespolri</Typography>
            </td>
            <td>
              <Typography className="text-11">12345678</Typography>
            </td>
          </tr>
        </tbody>
      </table>
      <ToastContainer autoClose={2000} />
    </div>
  );
}

export default MabesLoginTab;
