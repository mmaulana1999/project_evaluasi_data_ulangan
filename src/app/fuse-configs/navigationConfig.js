import i18next from 'i18next';
import ar from './navigation-i18n/ar';
import en from './navigation-i18n/en';
import tr from './navigation-i18n/tr';

i18next.addResourceBundle('en', 'navigation', en);
i18next.addResourceBundle('tr', 'navigation', tr);
i18next.addResourceBundle('ar', 'navigation', ar);

const navigationConfig = [
  {
    id: 'applications',
    title: 'Applications',
    translate: 'APPLICATIONS',
    type: 'group',
    icon: 'apps',
    children: [
      {
        id: 'example-component',
        title: 'Dashboard',
        translate: 'Dashboard',
        type: 'item',
        icon: 'dashboard',
        url: 'dashboard',
      },
      {
        id: 'example-component',
        title: 'Admin',
        translate: 'Admin',
        type: 'item',
        icon: 'home',
        url: 'admin',
      },
      {
        id: 'example-component',
        title: 'Super Admin',
        translate: 'SuperAdmin',
        type: 'item',
        icon: 'whatshot',
        url: 'superAdmin',
      },
      // {
      //   id: 'example-component',
      //   title: 'Example',
      //   translate: 'EXAMPLE',
      //   type: 'item',
      //   icon: 'whatshot',
      //   url: 'example',
      // },
      // {
      //   id: 'crud',
      //   title: 'Crud',
      //   translate: 'Crud',
      //   type: 'item',
      //   icon: 'whatshot',
      //   url: 'crud',
      // },
      // {
      //   id: 'infinity',
      //   title: 'Infinity',
      //   translate: 'infinity',
      //   type: 'item',
      //   icon: 'whatshot',
      //   url: 'infinity',
      // },
      // {
      //   id: 'reactLazyLoad',
      //   title: 'ReactLazyLoad',
      //   translate: 'reactLazyLoad',
      //   type: 'item',
      //   icon: 'whatshot',
      //   url: 'reactLazyLoad',
      // },
    ],
  },
];

export default navigationConfig;
